<?php
get_header();
$query = get_queried_object();
$posts = new WP_Query([
	'posts_per_page' => 9,
	'post_type' => 'project',
	'suppress_filters' => false,
	'tax_query' => array(
		array(
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'project',
	'suppress_filters' => false,
	'tax_query' => array(
		array(
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
?>
	<article class="page-body">
		<div class="main-category-image-block mt-3">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="main-category-image" <?php if ($cat_img = get_field('cat_img', $query)) : ?>
							style="background-image: url('<?= $cat_img['url']; ?>')"
						<?php endif; ?>>
							<div class="cat-page-data">
								<div class="row justify-content-between">
									<div class="col-auto">
										<h1 class="category-page-name">
											<?= $query->name; ?>
										</h1>
									</div>
									<div class="col-auto">
										<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container container-small-layout">
			<div class="row justify-content-center">
				<div class="col-xl-9 col-lg-8 col-12">
					<div class="cats-back">
						<div class="cats-back-padding">
							<?php if ($posts->have_posts()) : ?>
								<div class="row justify-content-center">
									<div class="col">
										<h1 class="base-title mb-4">
											<?= $query->name; ?>
										</h1>
										<div class="base-output">
											<?= category_description(); ?>
										</div>
									</div>
								</div>
								<div class="row justify-content-center align-items-stretch put-here-posts">
									<?php foreach ($posts->posts as $term) {
										get_template_part('views/partials/card', 'project_col',
											[
												'post' => $term,
											]);
									} ?>
								</div>
								<?php if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 9)) : ?>
									<div class="row justify-content-center mt-4">
										<div class="col-auto">
											<div class="more-link base-link load-more-posts" data-type="project" data-count="<?= $num; ?>"
												 data-term="<?= $query->term_id; ?>" data-term_name="project_cat">
												טען עוד..
											</div>
										</div>
									</div>
								<?php endif; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-12 sticky-form-col">
					<div class="sticky-form">
						<?php get_template_part('views/partials/repeat', 'form_vertical'); ?>
					</div>
				</div>
			</div>
			<div class="row justify-content-start">
				<div class="col-xl-9 col-lg-8 col-12">
					<?php if ($seo = get_field('single_slider_seo', $query)) {
						get_template_part('views/partials/content', 'slider', [
							'content' => $seo,
							'img' => get_field('slider_img', $query),
						]);
					} ?>
				</div>
			</div>
		</div>
	</article>
<?php get_footer(); ?>





<?php get_footer(); ?>
