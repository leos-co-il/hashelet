<?php
/*
Template Name: סוגי השלטים
*/

get_header();
$fields = get_fields();
$terms = get_terms([
		'taxonomy'      => 'project_cat',
		'hide_empty'    => false,
		'parent'        => 0
]);
get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
<article class="page-body">
	<div class="container container-small-layout pt-4">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-lg-8 col-12">
				<div class="cats-back">
					<div class="cats-back-padding">
						<?php if ($terms) : ?>
							<div class="row justify-content-center">
								<div class="col">
									<h1 class="base-title mb-4">
										<?php the_title(); ?>
									</h1>
								</div>
							</div>
							<div class="row justify-content-center align-items-stretch put-here-posts">
								<?php foreach ($terms as $term) {
									get_template_part('views/partials/card', 'category',
											[
													'post' => $term,
											]);
								} ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-4 col-12 sticky-form-col">
				<div class="sticky-form">
					<?php get_template_part('views/partials/repeat', 'form_vertical'); ?>
				</div>
			</div>
		</div>
		<div class="row justify-content-start">
			<div class="col-xl-9 col-lg-8 col-12">
				<?php if ($fields['single_slider_seo']) {
					get_template_part('views/partials/content', 'slider', [
							'content' => $fields['single_slider_seo'],
							'img' => $fields['slider_img'],
					]);
				} ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
