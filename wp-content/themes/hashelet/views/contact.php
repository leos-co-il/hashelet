<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$fax = opt('fax');
$map = opt('map_image');
$hours = opt('open_hours');
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-5 d-flex flex-column contacts-column">
				<h1 class="block-title mb-4">
					<?php the_title(); ?>
				</h1>
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
				<?php if ($tel) : ?>
					<div class="contact-item contact-item-link wow fadeInUp"
						 data-wow-delay="0.2s">
						<div class="contact-icon-wrap">
							<img src="<?= ICONS ?>contact-tel.png">
						</div>
						<div class="contact-info-wrap">
							<h4 class="contact-info-title">טלפון</h4>
							<a href="tel:<?= $tel; ?>" class="contact-info">
								<?= $tel; echo ($whatsapp = opt('whatsapp')) ? ' | '.$whatsapp : ''; ?>
							</a>
						</div>
					</div>
				<?php endif;
				if ($fax) : ?>
					<div class="contact-item wow fadeInUp" data-wow-delay="0.4s">
						<div class="contact-icon-wrap">
							<img src="<?= ICONS ?>contact-fax.png">
						</div>
						<div class="contact-info-wrap">
							<h4 class="contact-info-title">פקס</h4>
							<p class="contact-info">
								<?= $fax; ?>
							</p>
						</div>
					</div>
				<?php endif;
				if ($mail) : ?>
					<div class="contact-item contact-item-link wow fadeInUp"
						 data-wow-delay="0.6s">
						<div class="contact-icon-wrap">
							<img src="<?= ICONS ?>contact-mail.png">
						</div>
						<div class="contact-info-wrap">
							<h4 class="contact-info-title">מייל:</h4>
							<a href="mailto:<?= $mail; ?>" class="contact-info">
								<?= $mail; ?>
							</a>
						</div>
					</div>
				<?php endif; ?>
				<?php if ($address) : ?>
					<div class="contact-item-link contact-item wow fadeInUp" data-wow-delay="0.8s">
						<div class="contact-icon-wrap">
							<img src="<?= ICONS ?>contact-geo.png">
						</div>
						<div class="contact-info-wrap">
							<h4 class="contact-info-title">מיקום:</h4>
							<a class="contact-info" href="https://www.waze.com/ul?q=<?= $address; ?>">
								<?= $address; ?>
							</a>
						</div>
					</div>
				<?php endif;
				if ($hours) : ?>
					<div class="contact-item wow fadeInUp" data-wow-delay="1s">
						<div class="contact-icon-wrap">
							<img src="<?= ICONS ?>contact-hours.png">
						</div>
						<div class="contact-info-wrap">
							<h4 class="contact-info-title">זמני פעילות שירות לקוחות:</h4>
							<p class="contact-info">
								<?= $hours; ?>
							</p>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-xl-7">
				<div class="contact-form-wrap form-wrapper">
					<?php if ($fields['contact_form_title']) : ?>
						<h2 class="form-title">
							<?= $fields['contact_form_title']; ?>
						</h2>
					<?php endif;
					getForm('11'); ?>
				</div>
			</div>
		</div>
		<?php if ($map) : ?>
			<div class="row justify-content-center">
				<div class="col-12 mb-4">
					<a class="map-image" href="<?= $map['url']; ?>" data-lightbox="map">
						<img src="<?= $map['url']; ?>" alt="map">
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php get_footer(); ?>
