<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="cat-item more-card" data-id="<?= $args['post']->ID; ?>">
		<a class="cat-item-image" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif;?>>
		</a>
		<div class="bordered-card">
			<a href="<?= $link; ?>" class="base-link card-name-link post-item-link">
				<?= $args['post']->post_title; ?>
			</a>
		</div>
	</div>
<?php endif; ?>
