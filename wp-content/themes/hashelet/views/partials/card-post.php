<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-lg-3 col-sm-6 col-12 post-col post-col-base">
		<div class="post-item more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-item-image" href="<?= $link; ?>">
				<?php if (has_post_thumbnail($args['post'])) : ?>
					<img src="<?= postThumb($args['post']); ?>" alt="post-image">
				<?php endif;?>
			</a>
			<div class="post-item-content">
				<a class="post-item-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				<p class="post-item-text">
					<?= text_preview($args['post']->post_content, 12); ?>
				</p>
			</div>
			<a href="<?= $link; ?>" class="post-link align-self-end">
				המשך קריאה
			</a>
		</div>
	</div>
<?php endif; ?>
