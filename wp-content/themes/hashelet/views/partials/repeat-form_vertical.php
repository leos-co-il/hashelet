<?php
$f_title = (isset($args['title']) && $args['title']) ? $args['title'] : opt('post_form_title');
$f_subtitle = (isset($args['subtitle']) && $args['subtitle']) ? $args['subtitle'] : opt('post_form_subtitle');
$id = (isset($args['id']) && $args['id']) ? $args['id'] : '9';
?>
<div class="form-wrapper form-wrapper-vertical">
	<?php if ($f_title) : ?>
		<h3 class="post-form-title"><?= $f_title; ?></h3>
	<?php endif;
	if ($f_subtitle) : ?>
		<h4 class="post-form-subtitle"><?= $f_subtitle; ?></h4>
	<?php endif; ?>
	<?php getForm($id); ?>
</div>
