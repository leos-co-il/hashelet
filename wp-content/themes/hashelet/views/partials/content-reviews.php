<section class="reviews-block arrows-slider">
	<div class="container">
		<div class="row">
			<?php if (isset($args['text']) && $args['text']) : ?>
				<div class="col-12">
					<div class="base-output text-center mb-5">
						<?= $args['text']; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if (isset($args['reviews']) && $args['reviews']) : ?>
				<div class="col-12">
					<div class="reviews-slider" dir="rtl">
						<?php foreach ($args['reviews'] as $review) : ?>
							<div>
								<a href="<?= $review['url']; ?>" data-lightbox="reviews" class="reviews-slide">
									<img src="<?= $review['url']; ?>" alt="review">
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
