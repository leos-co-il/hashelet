<section class="clients-block arrows-slider">
	<div class="container">
		<div class="row">
			<?php if (isset($args['title']) && $args['title']) : ?>
				<div class="col-12">
					<div class="base-title text-center mb-3">
						<?= $args['title']; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if (isset($args['clients']) && $args['clients']) : ?>
				<div class="col-12">
					<div class="clients-slider" dir="rtl">
						<?php foreach ($args['clients'] as $review) : ?>
							<div class="client-slide">
								<img src="<?= $review['url']; ?>" alt="review">
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
