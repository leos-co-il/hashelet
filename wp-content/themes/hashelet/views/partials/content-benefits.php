<?php if (isset($args['benefits']) && $args['benefits']) : ?>
	<div class="benefits-block">
		<div class="container">
			<div class="row justify-content-center align-items-start">
				<?php foreach ($args['benefits']as $x => $why_item) : ?>
					<div class="col-lg-3 col-sm-6 col-12 why-item-col wow fadeInUp mb-5" data-wow-delay="0.<?= $x + 1; ?>s">
						<div class="why-item">
							<div class="why-icon-wrap">
								<?php if ($why_item['ben_icon']) : ?>
									<img src="<?= $why_item['ben_icon']['url']; ?>" alt="benefit-icon">
								<?php endif; ?>
							</div>
							<h3 class="why-item-title">
								<?= $why_item['ben_title']; ?>
							</h3>
							<div class="why-item-text">
								<?= $why_item['ben_desc']; ?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
