<?php if ( function_exists('yoast_breadcrumb') ) : ?>
	<div class="container bread-container">
		<div class="row justify-content-center bread-row mt-3">
			<div class="col-12">
				<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
			</div>
		</div>
	</div>
<?php endif; ?>
