<?php if (isset($args['post']) && $args['post']) : $link = get_term_link($args['post']); ?>
	<div class="col-lg-3 col-sm-6 col-12 post-col post-cat-base">
		<div class="cat-item more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="cat-item-image" href="<?= $link; ?>"
				<?php if ($img = get_field('cat_img', $args['post'])) : ?>
					style="background-image: url('<?= $img['url']; ?>')"
				<?php endif;?>>
			</a>
			<div class="bordered-card">
				<a href="<?= $link; ?>" class="base-link card-name-link">
					<?= $args['post']->name; ?>
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
