<?php
$f_title = (isset($args['title']) && $args['title']) ? $args['title'] : opt('base_form_title');
$f_subtitle = (isset($args['subtitle']) && $args['subtitle']) ? $args['subtitle'] : opt('base_form_subtitle');
$id = (isset($args['id']) && $args['id']) ? $args['id'] : '8';
?>
<section class="repeat-form-block">
	<div class="container">
		<div class="row">
			<?php if ($f_title) : ?>
				<div class="col-12">
					<h2 class="base-title text-center mb-3">
						<?= $f_title; ?>
					</h2>
				</div>
			<?php endif; ?>
			<div class="col-12">
				<div class="form-wrapper form-wrapper-horizontal">
					<?php if ($f_subtitle) : ?>
						<h3 class="form-title"><?= $f_subtitle; ?></h3>
					<?php endif;
					getForm($id); ?>
				</div>
			</div>
		</div>
	</div>
</section>
