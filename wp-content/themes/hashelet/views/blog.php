<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$type = $fields['type'] ? $fields['type'] : 'post';
$posts = new WP_Query([
	'posts_per_page' => 9,
	'post_type' => $type,
	'suppress_filters' => false
]);
$published_posts = new WP_Query([
		'posts_per_page' => -1,
		'post_type' => $type,
]);
get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
<article class="page-body">
	<div class="container container-small-layout pt-4">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-lg-8 col-12">
				<div class="cats-back">
					<div class="cats-back-padding">
						<?php if ($posts->have_posts()) : ?>
							<div class="row justify-content-center">
								<div class="col">
									<h1 class="base-title mb-4">
										<?php the_title(); ?>
									</h1>
									<div class="base-output text-center">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
							<div class="row justify-content-center align-items-stretch put-here-posts">
								<?php foreach ($posts->posts as $term) {
									get_template_part('views/partials/card', 'project_col',
											[
													'post' => $term,
											]);
								} ?>
							</div>
							<?php if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 9)) : ?>
								<div class="row justify-content-center mt-4">
									<div class="col-auto">
										<div class="more-link base-link load-more-posts" data-type="<?= $type; ?>" data-count="<?= $num; ?>">
											טען עוד..
										</div>
									</div>
								</div>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-4 col-12 sticky-form-col">
				<div class="sticky-form">
					<?php get_template_part('views/partials/repeat', 'form_vertical'); ?>
				</div>
			</div>
		</div>
		<div class="row justify-content-start">
			<div class="col-xl-9 col-lg-8 col-12">
				<?php if ($fields['single_slider_seo']) {
					get_template_part('views/partials/content', 'slider', [
							'content' => $fields['single_slider_seo'],
							'img' => $fields['slider_img'],
					]);
				} ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
