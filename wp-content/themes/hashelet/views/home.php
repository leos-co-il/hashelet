<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
if ($fields['main_slider']) : ?>
<section class="main-slider-block">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-11">
				<div class="main-slider" dir="rtl">
					<?php foreach ($fields['main_slider'] as $slide) : ?>
						<div class="main-slide-item" style="background-image: url('<?= $slide['url']; ?>')">
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;
get_template_part('views/partials/repeat', 'form_horizontal', [
		'title' => $fields['h_form_block_title'],
		'subtitle' => $fields['h_form_title_1'],
		'id' => '7',
]);
if ($fields['h_project_cats']) : ?>
	<section class="home-posts-block">
		<div class="container">
			<?php if ($fields['h_project_cats_title']) : ?>
				<div class="row">
					<div class="col">
						<h2 class="base-title mb-4">
							<?= $fields['h_project_cats_title']; ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['h_project_cats'] as $post) {
					get_template_part('views/partials/card', 'category',
							[
									'post' => $post,
							]);
				} ?>
			</div>
			<?php if (['h_project_cats_link']) : ?>
				<div class="row justify-content-center mt-4">
					<div class="col-auto">
						<a href="<?= $fields['h_project_cats_link']['url'];?>" class="base-link">
							<?= (isset($fields['h_project_cats_link']['title']) && $fields['h_project_cats_link']['title'])
									? $fields['h_project_cats_link']['title'] : 'לכל סוגי השלטים';
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
<section class="about-home">
	<?php if ($fields['h_about_text'] || $fields['h_about_video']) : ?>
		<div class="container mb-5">
			<div class="row justify-content-center">
				<div class="col-10">
					<div class="row justify-content-between align-items-center">
						<div class="<?= $fields['h_about_video'] ? 'col-lg-6 col-12' : 'col-12'; ?>">
							<div class="base-output">
								<?= $fields['h_about_text']; ?>
							</div>
							<?php if ($fields['h_about_link']) : ?>
								<div class="row justify-content-end">
									<div class="col-auto mt-3">
										<a href="<?= isset($fields['h_about_link']['url']) ? $fields['h_about_link']['url'] : ''; ?>" class="base-link">
											<?= isset($fields['h_about_link']['title']) ? $fields['h_about_link']['title'] : 'קרא עוד עלינו'; ?>
										</a>
									</div>
								</div>
							<?php endif; ?>
						</div>
						<?php if ($fields['h_about_video']) : ?>
							<div class="col-xl-5 col-lg-6 col-12">
								<div class="image-border">
									<div class="image-video-inside">
								<span class="play-video" data-video="<?= getYoutubeId($fields['h_about_video']); ?>">
									<?= svg_simple(ICONS.'play.svg'); ?>
									<span class="put-video-here"></span>
								</span>
										<img src="<?= getYoutubeThumb($fields['h_about_video']); ?>" alt="about-us">
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif;
	if ($fields['h_about_benefits']) {
		get_template_part('views/partials/content', 'benefits', [
				'benefits' => $fields['h_about_benefits'],
		]);
	} ?>
</section>
<?php if ($fields['h_projects']) : ?>
	<section class="home-posts-block mt-5">
		<div class="container">
			<?php if ($fields['h_projects_title']) : ?>
				<div class="row">
					<div class="col">
						<h2 class="base-title mb-4">
							<?= $fields['h_projects_title']; ?>
						</h2>
					</div>
				</div>
			<?php endif;
			if ($fields['h_projects_link']) : ?>
				<div class="row justify-content-center">
					<?php foreach ($fields['h_projects_link'] as $link) : ?>
						<div class="col-auto">
							<a href="<?= $link['url']; ?>">
								<?= (isset($link['title']) && $link['title']) ? $link['title'] : '';
								?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch home-projects-row">
				<?php foreach ($fields['h_projects'] as $post) : ?>
					<div class="col-xxl-post col-xl-3 col-lg-4 col-sm-6 col-12 post-cat-base">
						<?php get_template_part('views/partials/card', 'project', [
								'post' => $post,
							]); ?>
					</div>
				<?php endforeach; ?>
			</div>
			<?php if ($fields['h_projects_link']) : ?>
				<div class="row justify-content-center mt-4">
					<div class="col-auto">
						<a href="<?= $fields['h_projects_link']['url'];?>" class="base-link">
							<?= (isset($fields['h_projects_link']['title']) && $fields['h_projects_link']['title'])
									? $fields['h_projects_link']['title'] : 'לכל הפרויקטים';
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'form_horizontal', [
		'title' => $fields['h_form_block_title'],
		'subtitle' => $fields['h_form_title_1'],
		'id' => '7',
]);
if ($fields['h_posts']) : ?>
	<section class="home-posts-block">
		<div class="container">
			<?php if ($fields['h_posts_title']) : ?>
				<div class="row">
					<div class="col">
						<h2 class="base-title mb-4">
							<?= $fields['h_posts_title']; ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['h_posts'] as $post) {
					get_template_part('views/partials/card', 'post',
							[
									'post' => $post,
							]);
				} ?>
			</div>
			<?php if (['h_posts_link']) : ?>
				<div class="row justify-content-center mt-4">
					<div class="col-auto">
						<a href="<?= $fields['h_posts_link']['url'];?>" class="base-link">
							<?= (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title'])
									? $fields['h_posts_link']['title'] : 'לכל הכתבות';
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['h_reviews_title'] || $fields['h_reviews']) {
	get_template_part('views/partials/content', 'reviews', [
			'title' => $fields['h_reviews_title'],
			'reviews' => $fields['h_reviews']
	]);
}
if ($fields['h_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['h_slider_seo'],
			'img' => $fields['h_slider_img'],
	]);
}
if ($fields['h_brands_slider'] || $fields['faq_item']) : ?>
	<section class="brands-faq-block">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<?php if ($fields['faq_item']) : ?>
					<div class="<?= $fields['h_brands_slider'] ? 'col-xl-5 col-lg-6 col-12' : 'col-12'; ?> mb-lg-0 mb-5">
						<?php if ($fields['faq_title']) : ?>
							<h3 class="faq-title">
								<?= $fields['faq_title']; ?>
							</h3>
						<?php endif; ?>
						<div id="accordion" class="faq">
							<?php foreach ($fields['faq_item'] as $num => $item) : ?>
								<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
									<div class="question-header" id="heading_<?= $num; ?>">
										<button class="question-title" data-toggle="collapse"
												data-target="#faqChild<?= $num; ?>"
												aria-expanded="false" aria-controls="collapseOne">
											<span class="base-text"><?= $item['faq_title']; ?></span>
											<span class="faq-icon plus-icon">+</span>
											<span class="faq-icon minus-icon">-</span>
										</button>
										<div id="faqChild<?= $num; ?>" class="collapse faq-item answer-body"
											 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
											<div class="base-output slider-output">
												<?= $item['faq_answer']; ?>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif;
				if ($fields['h_brands_slider']) : $brands = array_chunk($fields['h_brands_slider'], 2); ?>
					<div class="<?= $fields['faq_item'] ? 'col-lg-6 col-12' : 'col-12'; ?> home-brands-col arrows-slider">
						<?php if ($fields['h_brands_title']) : ?>
							<h2 class="base-title text-center mb-3"><?= $fields['h_brands_title']; ?></h2>
						<?php endif; ?>
						<div class="brands-slider" dir="rtl">
							<?php foreach ($brands as $brand_item) : ?>
								<div>
									<div class="brand-col">
										<?php foreach ($brand_item as $brand) : ?>
											<div class="brand-item">
												<img src="<?= $brand['url']; ?>" alt="brand">
											</div>
										<?php endforeach; ?>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) : ?>
	<section class="reverse-slider">
		<?php get_template_part('views/partials/content', 'slider', [
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_img'],
		]); ?>
	</section>
<?php endif;
get_footer(); ?>
