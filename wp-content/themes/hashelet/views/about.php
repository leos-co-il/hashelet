<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
$video = $fields['about_video'];
$image = has_post_thumbnail() ? postThumb() : '';
get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
<article class="page-body">
	<div class="container mb-5">
		<div class="row justify-content-between">
			<div class="<?= $image || $video ? 'col-lg-6 col-12' : 'col-12'; ?>">
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
			<?php if ($video || $image) : ?>
				<div class="col-xl-5 col-lg-6 col-12">
					<?php if ($video) : ?>
						<div class="image-border">
							<div class="image-video-inside">
								<span class="play-video" data-video="<?= getYoutubeId($video); ?>">
									<?= svg_simple(ICONS.'play.svg'); ?>
									<span class="put-video-here"></span>
								</span>
								<img src="<?= getYoutubeThumb($video); ?>" alt="about-us">
							</div>
						</div>
					<?php else: ?>
						<div class="image-border">
							<img src="<?= $image; ?>" alt="about-us">
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if ($fields['about_benefits']) {
		get_template_part('views/partials/content', 'benefits', [
				'benefits' => $fields['about_benefits'],
		]);
	} ?>
</article>
<?php get_template_part('views/partials/repeat', 'form_horizontal', [
		'title' => $fields['h_about_form_block'],
		'subtitle' => $fields['h_about_form_title'],
		'id' => '8',
]);
if ($fields['about_reviews_text'] || $fields['about_reviews']) {
	get_template_part('views/partials/content', 'reviews', [
			'text' => $fields['about_reviews_text'],
			'reviews' => $fields['about_reviews']
	]);
}
if ($fields['about_clients']) {
	get_template_part('views/partials/content', 'clients', [
			'title' => $fields['about_clients_title'],
			'clients' => $fields['about_clients']
	]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
