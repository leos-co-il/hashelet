<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif;
$whatsapp = opt('whatsapp');
$facebook = opt('facebook');
$youtube = opt('youtube'); ?>
<header class="header-home sticky">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-11 col-12">
				<div class="row align-items-center">
					<div class="col-lg-2 col-auto col d-flex justify-content-start header-logo-col">
						<?php if ($logo = opt('logo')) : ?>
							<a href="/" class="logo">
								<img src="<?= $logo['url']; ?>" alt="hashelet-logo">
							</a>
						<?php endif; ?>
					</div>
					<div class="col-md col-auto col-menu-header">
						<nav id="MainNav" class="h-100">
							<div id="MobNavBtn">
								<span></span>
								<span></span>
								<span></span>
							</div>
							<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
						</nav>
					</div>
					<div class="col-xl-3 col-md-auto col header-contacts">
						<?php if ($tel = opt('tel')) : ?>
							<a href="tel:<?= $tel; ?>" class="header-tel">
								<?= $tel ? $tel : ''; echo $whatsapp ? ' | '.$whatsapp : ''; ?>
							</a>
						<?php endif;
						if ($mail = opt('mail')) : ?>
						<a href="mailto:<?= $mail; ?>" class="header-mail">
							<?= $mail; ?>
						</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="pop-great">
	<div class="float-form">
		<?php get_template_part('views/partials/repeat', 'form_vertical', [
				'title' => opt('pop_form_title'),
				'subtitle' => opt('pop_form_subtitle'),
				'id' => '6',
		]); ?>
	</div>
</div>
<div class="triggers-col">
	<?php if ($whatsapp) : ?>
		<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="trigger-item whatsapp-item">
			<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp">
		</a>
	<?php endif;
	if ($facebook) : ?>
		<a href="<?= $facebook; ?>" class="trigger-item facebook-item">
			<img src="<?= ICONS ?>facebook.png" alt="facebook">
		</a>
	<?php endif;
	if ($youtube) : ?>
		<a href="<?= $youtube; ?>" class="trigger-item youtube-item">
			<img src="<?= ICONS ?>youtube.png" alt="youtube">
		</a>
	<?php endif; ?>
	<div class="trigger-item pop-trigger">
		<img src="<?= ICONS ?>mail.png" alt="pop-trigger">
	</div>
</div>
