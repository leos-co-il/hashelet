<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
<article class="post-body">
	<?php if ($fields['slider_post']) : ?>
	<div class="main-slider-block post-slider-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="main-slider" dir="rtl">
						<?php foreach ($fields['slider_post'] as $slide) : ?>
							<div class="main-slide-item" style="background-image: url('<?= $slide['url']; ?>')">
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-between">
			<div class="col-xl-8 col-lg-7 col-12">
				<div class="base-output post-output">
					<?php the_content(); ?>
				</div>
				<div class="socials-share">
					<span class="share-text">
						שתף
					</span>
					<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.2s">
						<img src="<?= ICONS ?>share-facebook.png">
					</a>
					<!--	WHATSAPP-->
					<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.4s">
						<img src="<?= ICONS ?>share-whatsapp.png">
					</a>
					<!--	MAIL-->
					<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.6s">
						<img src="<?= ICONS ?>share-mail.png">
					</a>
				</div>
			</div>
			<div class="col-xl-4 col-lg-5 col-12 page-form-col-post">
				<?php get_template_part('views/partials/repeat', 'form_vertical');
				if (has_post_thumbnail()) : ?>
					<img src="<?= postThumb(); ?>" alt="post-image" class="post-page-img w-100">
				<?php endif; ?>
			</div>
		</div>
		<?php if ($fields['post_gallery']) : ?>
			<div class="row mt-5">
				<div class="col-12">
					<h2 class="base-title mb-4">
						גלריה
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['post_gallery'] as $img) : ?>
					<div class="col-lg-3 col-sm-6 col-12 gallery-col">
						<div class="gallery-item" style="background-image: url('<?= $img['url']; ?>')">
							<a href="<?= $img['url']; ?>"  data-lightbox="post-gallery" class="gallery-overlay">
								+
							</a>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</article>

<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
